clear
reset
set term postscript eps enhanced color solid font 'Helvetica,38' size 10in,6in
set output "Ener-WC.eps"
set ylabel "Normalized energy consumption" offset 2,0
set xtics in nomirror
set autoscale y
set yrange [0:]
set ytics in nomirror
set grid ytics lt 0 lw 1 lc rgb "grey40"
#set key center right
set style data histogram
set style histogram clustered gap 1 errorbars gap 1 lw 6
set style fill solid 0.5 border 3
set key inside top left horizontal
#set xtics rotate by -30
set xtics offset -1,0
set key at -0.8,1.55
set offset -0.6,-0.6,0,0
set xlabel offset -3,0
#set bmargin 5

baseline=183389.80

plot '../dataset/wc-ener.txt' u ($2/baseline):($3/baseline):($4/baseline):xticlabels(1) title "Default", '' u ($5/baseline):($6/baseline):($7/baseline):xticlabels(1) title "Hierarchical", '' u ($8/baseline):($9/baseline):($10/baseline):xticlabels(1) title "LATE"
#, '' u ($11/baseline):($12/baseline):($13/baseline):xticlabels(1) title "LATE"


