clear
reset
set term postscript eps enhanced color font 'Helvetica,35' size 6in,5in
set output "FakePositive-New.eps"
set ylabel "Fake positive ratio" offset 2,0
set xlabel "Speculation lag (sec)"
set yrange [0:]
set xrange [10:70]
set autoscale xfixmin
set autoscale xfixmax
set grid ytics lt 0 lw 1 lc rgb "grey40"
set grid xtics lt 0 lw 1 lc rgb "grey40"
set key inside top left vertical
set tmargin 1.5

plot "../dataset/fake-positive.txt" using 1:2 with lines notitle lt 2 lw 8