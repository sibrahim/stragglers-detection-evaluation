clear
reset
set term postscript eps enhanced color solid font 'Helvetica,38' size 10in,6in
set output "Spec-WC.eps"
set bmargin 3

set ylabel "Number of speculative tasks" offset 1.5,0
set xlabel offset 0,1
set xtics in nomirror
set autoscale y
set yrange [0:]
set grid ytics lt 0 lw 1 lc rgb "grey40"
set style data histogram
set style histogram rowstacked 
set style fill solid 0.5 border 3
set key inside top left
set key at -0.5,240
set offset -0.49,-0.49,0,0

set label "100" at 0.5,-25
set label "95" at 4,-25
set label "90" at 8.5,-25
set label "75" at 12.5,-25
set label "50" at 16.5,-25
set label "Shared"	at 20.5,-25
#set label "0-0-10-90"	at 23.7,-25
#set label "5-5-0-90"	at 27.8,-25

set xtics norangelimit font ",28"

plot newhistogram "", '../dataset/wc-spec.txt' every ::0::2 using 5:xtic(1) title "Successful speculative tasks" lt 2, '' every ::0::2  u ($2-$5) title "Unsuccessful speculative tasks" lt 1, newhistogram "", '../dataset/wc-spec.txt' every ::3::5  using 5:xtic(1) notitle lt 2, '' every ::3::5  u ($2-$5) notitle lt 1, newhistogram "", '../dataset/wc-spec.txt' every ::6::8  using 5:xtic(1) notitle lt 2, '' every ::6::8  u ($2-$5) notitle lt 1, newhistogram "", '../dataset/wc-spec.txt' every ::9::11  using 5:xtic(1) notitle lt 2, '' every ::9::11  u ($2-$5) notitle lt 1, newhistogram "", '../dataset/wc-spec.txt' every ::12::14  using 5:xtic(1) notitle lt 2, '' every ::12::14  u ($2-$5) notitle lt 1, newhistogram "", '../dataset/wc-spec.txt' every ::15::17  using 5:xtic(1) notitle lt 2, '' every ::15::17  u ($2-$5) notitle lt 1  #, newhistogram "", '../dataset/wc-spec.txt' every ::18::20  using 5:xtic(1) notitle lt 2, '' every ::18::20  u ($2-$5) notitle lt 1, newhistogram "", '../dataset/wc-spec.txt' every ::21::23  using 5:xtic(1) notitle lt 2, '' every ::21::23  u ($2-$5) notitle lt 1
