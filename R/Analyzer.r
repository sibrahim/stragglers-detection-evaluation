library(ggplot2)

key = 0.5 		#defines the quartile limit.
st_def = 1.2	#defines the factor to become a straggler


applis <- c("../dataset/scenario-35-35-5-25-2.txt","../dataset/scenario-25-25-25-25-2.txt","../dataset/scenario-10-10-5-75-2.txt","../dataset/scenario-5-5-0-90-2.txt")
applis_ref <- c("../dataset/homo.txt","../dataset/homo.txt", "../dataset/homo.txt", "../dataset/homo.txt")
strategies = c("D","DH","L","LH")

for (j in 1:4 ) {
	dat = read.table(applis[j],header = FALSE)
	ref = read.table(applis_ref[1],header = FALSE)
	cat(sep = "",applis[j],"\n")
	cat(sep = "\t",
		"Strategy",
		 "&", "Precision",
		 "&", "Recall",
		 "&", "Response Time",
		 "&", "Fake Positive",
		 "&", "Undetected Time",
		"\\\\\n"
	)
	cat(sep = "",
		"\\hline",
		"\n"
	)
	for (k in 1:4){
		dat_strat = subset(dat,V5==strategies[k])

		false_pos = 0
		true_pos = 0
		false_neg = 0
		response_time = 0
		fake_pos = 0

		undetected_timeB = 0
		
		
		stragglers = 0
		total = 0
		for (l in 0:max(ref$V2)) {
			x = subset(dat_strat,V2==l)
			x_ref = subset(ref,V2==l)

			usual_time = quantile(x_ref$V3,key,names=F)

			i = usual_time*st_def
#            cat(sep = "\n",l, i)

			true_pos = true_pos + sum(x$V3>i & x$V4 == "Y")
			false_neg = false_neg + sum(x$V3>i &  x$V4 == "N")
			
			fake_pos = fake_pos +  sum(x$V3>i &  x$V4 == "Y" & (x$V3 - x$V6 < i))
			
			stragglers = stragglers + sum(x$V3>i)
			total = total + sum(x$V3>0)

			x_time = subset(x, (V3>i & V4 == "Y"& (x$V3 - x$V6 > i)))
			response_time = response_time + sum(x_time$V6) / usual_time

			x_time = subset(x, (V3>i & V4 == "Y" & (x$V3 - x$V6 < i)))
			undetected_timeB = undetected_timeB + sum(x_time$V3) / usual_time

			x_time = subset(x, (V3>i & V4 == "N" ))
			undetected_timeB = undetected_timeB + sum(x_time$V3) / usual_time

			
			false_pos = false_pos + sum(x$V4 == "Y" & x$V3<=i)
		}
		cat(sep = "\t",
					strategies[k],
                    "&", signif((true_pos)/(true_pos + false_pos),2), #precision
                    #"&", signif((true_pos-fake_pos)/(true_pos + false_pos),2), #precision
                    "&", signif((true_pos)/(true_pos + false_neg),2), #recall
                    #"&", signif((true_pos-fake_pos)/(true_pos + false_neg),2), #recall
					"&", signif(response_time/true_pos,2), #response time
					"&", signif(fake_pos/(true_pos + false_neg),2),
					"&", signif(undetected_timeB/(false_neg+fake_pos),2),
					"\\\\\n"
				)
	}
	cat(sep = "\t",
				"Clone",
				"&", signif(stragglers/total,2),
				"&", 1,
				"&", 0,
				"&", 0,
				"&", "0",
				"\n"
			)

}

	


homo = read.table("../dataset/homo.txt",header = FALSE)
key=0.5
stragglers =0
total = 0
for (j in 0:max(homo$V1)) {
	x=subset(homo,V2==j)
#	i= (mean(x$V3))*1.2
	i= (quantile(x$V3,key,names=F))
	stragglers = stragglers + sum(x$V3>i)
	total =total+ sum(x$V3>0)
}
print(stragglers)
print(stragglers/total)

readable = subset(homo, (V2 <100))

ggplot(km_homo, aes(x=V2, y=V3, fill=as.factor(V2))) + geom_boxplot()+guides(fill=FALSE)+xlab("Task ID")+ylab("Execution time")









homo = read.table("../dataset/homo.txt",header = FALSE)
ggplot(homo, aes(x=V2, y=V3, fill=as.factor(V2))) + geom_boxplot()+guides(fill=FALSE)+xlab("Task ID")+ylab("Execution time")






dat = read.table("../dataset/homo.txt",header = FALSE)
ggplot(dat, aes(x=V2, y=V3, fill=as.factor(V2))) + geom_boxplot()+guides(fill=FALSE)+
	scale_y_log10("time",  limits=c(15000,200000),breaks=(2:15)*10**(4) )

dat = read.table("../dataset/homo.txt",header = FALSE)
for (j in 0) {
	x=subset(dat,V2==j)
	cat(sep = "\t&\t",
		format(quantile(x$V3,0.25,names=F), big.mark=","),
		format(quantile(x$V3,0.5,names=F), big.mark=","),
		format(quantile(x$V3,0.6,names=F), big.mark=","),
		format(quantile(x$V3,0.75,names=F), big.mark=","),
		"\\\\\n"
	)
}

ggplot(dat, aes(x=V2, y=V3, fill=as.factor(V2))) + geom_boxplot()+guides(fill=FALSE)
	scale_y_log10("time",  limits=c(15000,200000),breaks=(2:15)*10**(4) )


library(ggplot2)
wc_homo = read.table("../dataset/homo.txt",header = FALSE)

stragglers =0
total = 0
for (j in 0:max(wc_homo$V2)) {
	x=subset(wc_homo,V2==j)
	i= (quantile(x$V3,0.5,names=F))*1.2
	stragglers = stragglers + sum(x$V3>i)
	total =total+ sum(x$V3>0)
}
print(stragglers)
print(total)
print(stragglers/total)




ggplot(wc_homo, aes(x=V2, y=V3, fill=as.factor(V2))) + geom_boxplot()+guides(fill=FALSE)+xlab("Application ID")+ylab("Execution time")+ylim(10000,40000)
ggsave("wc-homo.pdf", width=20, height=5,device=cairo_pdf)

